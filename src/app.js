const Hapi = require('@hapi/hapi');
const path =require('path');
const inert =require('inert');
const handlebars=require('handlebars')
const user =require('./models/users')
//database
require('../database');


const init = async ()=>{
    const server = new Hapi.Server({
        port:4000,
        host:'localhost',
        routes:{
            files:{
                relativeTo:path.join(__dirname,'public')
            }
        }
    });

   await server.register(inert);
   await server.register(require('@hapi/vision'))
   server.views({
        engines:{
            html:handlebars
        },
        relativeTo:__dirname,
        path:'templates',
        isCached:process.env.NODE_ENV==='production',
        
    })

   await server.start();

   console.log(`server running on  ${server.info.uri}`);

   server.route({
       method:'GET',
       path:'/',
       handler:(request,h)=>{
           return '<h1>Hello world!</h1>';
       },
   })

   server.route({
       method:'GET',
       path:'/about',
       handler:(request,h)=>{
        return 'About!';
    },
   })
   
   server.route({
       method:'GET',
       path:'/text',
       handler:(request,h)=>{
           return h.file('./text.txt');
       }
   })

   server.route({
       method:'GET',
       path:'/hello/{user}',
       handler:(request,h)=>{
           const {user}=request.params
           return `<h1>Hello ${user}</h1>
           <h2>la ley del monte esta vaina</h2>
           `;
       }
   })

   server.route({
       method:'GET',
       path:'/page',
       handler:(request,h)=>{
           return h.view('index')
       }
   })

   server.route({
    method:'GET',
    path:'/name/{name}',
    handler:(request,h)=>{
        return h.view('name',{
            name:request.params.name
        })
    }
})
    server.route({
        method:'GET',
        path:'/products',
        handler:(request,h)=>{
            return h.view('products',{
                products:[
                    {name:'lp'},
                    {name:'lp3'},
                    {name:'l2p'},
                    {name:'lp5'}
                ]
            })
        
    }
    })

    server.route({
        method:'GET',
        path:'/user',
        handler:async (request,h)=>{
           const users= await user.find();
            return h.view('users',{users});
        }
    })

    server.route({
        method:'POST',
        path:'/user',
        handler:async (request,h)=>{

            const {username}=request.payload;
            const newUser = new user({
                username:username,
            })
            console.log(newUser);
            await newUser.save();
            return h.redirect().location('user');
        }
    })
}

init();